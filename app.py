import sys
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QTextEdit
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtSql import *
from PyQt5.QtSql import QSqlDatabase
from PyQt5.QtSql import QSqlDatabase, QSqlQuery, QSqlTableModel
from state_widget import Ui_Form
from add_students_window import Ui_Dialog




class StateWidget(QtWidgets.QWidget, Ui_Form):

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.setupUi(self)
        self.window2 = None
        self.window3 = None
        print(list(map(str, QSqlDatabase.drivers())))
        print(QApplication.libraryPaths())

        # Connect DB
        """self.db = QSqlDatabase.addDatabase("QSQLITE")
        self.db.setDatabaseName("Students.db")"""
        self.model = QSqlTableModel()
        self.initializedModel()
        self.table.setModel(self.model)
        self.model.select()

        # Make a model

        # Connect model with table
        self.edit_students.clicked.connect(self.add_student)
        # self.table.doubleClicked.connect(self.edit)

    # initialisation

    def initializedModel(self):
        self.model.setTable("Students_list")
        #self.model.setEditStrategy(QSqlTableModel.OnFieldChange)
        self.model.setHeaderData(0, Qt.Horizontal, "ID")
        self.model.setHeaderData(1, Qt.Horizontal, "Name")
        self.model.setHeaderData(2, Qt.Horizontal, "Surname")
        #self.model.setHeaderData(3, Qt.Horizontal, "Groups")

    def add_student(self):
        if self.window2 is None:
            self.window2 = Add()
            self.window2.main_window = self
        self.window2.show()

    def add_student_end(self,name,s_name):
        cur = QSqlQuery()
        cur.exec_(f"""INSERT INTO Students_list(name, surname)
                    VALUES ('{name}' ,'{s_name}')""" )
        self.model.select()



class Add(QtWidgets.QDialog, Ui_Dialog):
    def __init__(self, parent=None):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('Add')

        # connect with main window
        self.main_window = StateWidget()

        # button to save info
        self.add_student.clicked.connect(self.add_student_f)

    def add_student_f(self):
        # Get Text
        name = self.name_line.text()
        s_name = self.surname_line.text()
        
        # Add to table
        self.main_window.add_student_end(name, s_name)
        # Clear Table
        self.name_line.clear()
        self.surname_line.clear()
        self.select_group.clear()
        self.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    db = QSqlDatabase.addDatabase("QSQLITE")
    db.setDatabaseName("Students.db")
    demo = StateWidget()
    demo.show()
    sys.exit(app.exec_())