import sys
from state_window import Ui_MainWindow
import add
import redactor
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5 import QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QTextEdit
from PyQt5.QtCore import pyqtSignal


class StateWindow(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.setupUi(self)
        self.setupUi(self)
        self.window2 = None
        self.window3 = None

        # Make a model
        #self.table.setHorizontalHeaderLabels(['Студент','Группа', 'з', 'д'])
        self.table.horizontalHeader().setVisible(False)
        self.table.verticalHeader().setVisible(False)
        self.table.setColumnCount(4)
        self.table.setRowCount(2)
        self.table.setSpan(0, 0, 2, 1)
        self.table.setSpan(0, 1, 2, 1)
        self.table.setSpan(0, 2, 1, 2)
        self.table.setItem(0, 0, QtWidgets.QTableWidgetItem('Студент'))
        self.table.setItem(0, 1, QtWidgets.QTableWidgetItem('Группа'))
        self.table.setItem(0, 2, QtWidgets.QTableWidgetItem('№ 1'))
        self.table.setItem(1, 2, QtWidgets.QTableWidgetItem('з'))
        self.table.setItem(1, 3, QtWidgets.QTableWidgetItem('д'))
        self.table.item(0, 0).setFlags(QtCore.Qt.ItemIsEnabled)
        self.table.item(0, 1).setFlags(QtCore.Qt.ItemIsEnabled)
        self.table.item(0, 2).setFlags(QtCore.Qt.ItemIsEnabled)
        self.table.item(1, 2).setFlags(QtCore.Qt.ItemIsEnabled)
        self.table.item(1, 3).setFlags(QtCore.Qt.ItemIsEnabled)
        self.table.resizeColumnsToContents()

        # Connect model with table
        self.edit_students.clicked.connect(self.add_student)
        self.table.doubleClicked.connect(self.edit)

    def add_student(self):
        if self.window2 is None:
            self.window2 = Add()
            self.window2.main_window = self
        self.window2.setWindowModality(Qt.ApplicationModal)
        self.window2.show()

    def edit(self, indexes):
        print(indexes.column(), indexes.row())
        if indexes.column() >= 2 or indexes.row() < 2:
            pass
        else:
            if self.window3 is None:
                self.window3 = Redactor()
                self.window3.main_window = self
            self.window3.setWindowModality(Qt.ApplicationModal)

            #Give info in new frame and full qlineedit
            self.window3.selected_row = indexes.row()
            student = self.window3.main_window.table.item(
                self.window3.selected_row, 0)
            student = student.text()
            group = self.window3.main_window.table.item(
                self.window3.selected_row, 1)
            group = group.text()
            name = student[: student.find(" ")]
            second_name = student[student.find(" ") + 1:]
            self.window3.name.setText(name)
            self.window3.second_name.setText(second_name)
            self.window3.group.setText(group)

            self.window3.show()


class Add(QtWidgets.QMainWindow, add.Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('Add')

        # connect with main window
        self.main_window = StateWindow()

        # button to save info
        self.pushButton.clicked.connect(self.add_student)

    def add_student(self):
        # Get Text
        name = self.name.text()
        s_name = self.second_name.text()
        student = s_name+' '+name
        student = QtWidgets.QTableWidgetItem(student)
        group = self.group.text()
        group = QtWidgets.QTableWidgetItem(group)

        # Add to table
        self.main_window.table.insertRow(self.main_window.table.rowCount())
        row_count = self.main_window.table.rowCount()
        self.main_window.table.setItem(row_count-1, 0, student)
        self.main_window.table.setItem(row_count-1, 1, group)

        # Clear Table
        self.name.clear()
        self.second_name.clear()
        self.group.clear()
        self.close()


class Redactor (QtWidgets.QMainWindow, redactor.Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('Redactor')
        self.selected_row = None

        # connect with main window
        self.main_window = StateWindow()

        # button to save info
        self.pushButton.clicked.connect(self.save)

        # button to remove student from table
        self.pushButton_2.clicked.connect(self.remove)

    def save(self):
        # Delete old info
        self.main_window.table.takeItem(self.selected_row, 0)
        self.main_window.table.takeItem(self.selected_row, 1)

        # Get Text
        name = self.name.text()
        s_name = self.second_name.text()
        student = s_name+' '+name
        student = QtWidgets.QTableWidgetItem(student)
        group = self.group.text()
        group = QtWidgets.QTableWidgetItem(group)

        # Add to table
        self.main_window.table.setItem(self.selected_row, 0, student)
        self.main_window.table.setItem(self.selected_row, 1, group)

        # Clear Table
        self.name.clear()
        self.second_name.clear()
        self.group.clear()
        self.close()

    def remove(self):
        # Delete row
        self.main_window.table.removeRow(self.selected_row)

        # Clear Table
        self.name.clear()
        self.second_name.clear()
        self.group.clear()
        self.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    demo = StateWindow()
    demo.show()
    sys.exit(app.exec_())
