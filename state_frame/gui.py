import sys  # sys нужен для передачи argv в QApplication
import table
import add
import redactor
from PyQt5 import QtCore
from PyQt5 import QtGui
from PyQt5 import QtWidgets
from PyQt5 import QtSql
from PyQt5.QtWidgets import QApplication, QWidget, QComboBox, QHBoxLayout, QVBoxLayout, QFileDialog
from PyQt5.QtGui import QStandardItem, QStandardItemModel, QFont
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtSql import QSqlTableModel


class Table(QtWidgets.QMainWindow, table.Ui_MainWindow):
    def __init__(self):
        super(Table,self).__init__()
        self.setupUi(self)
        self.window2 = None
        self.window3 = None

        # Make a model
        self.students_table.setHorizontalHeaderLabels(['Студент','Группа', 'з', 'д'])

        # Connect model with table
        self.pushButton.clicked.connect(self.add_student)
        self.students_table.doubleClicked.connect(self.edit)

    def add_student(self):
        if self.window2 is None:
            self.window2 = Add()
            self.window2.main_window=self
        self.window2.show()

    def edit(self, indexes):
        if self.window3 is None:
            self.window3 = Redactor()
            self.window3.main_window=self

        print(indexes)
        self.window3.selected_row = indexes.row() 
        self.window3.show()





class Add(QtWidgets.QMainWindow, add.Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('Add')

        #connect with main window 
        self.main_window = Table()

        #button to save info 
        self.pushButton.clicked.connect(self.add_student)

        
    
    def add_student(self):
        # Get Text
        name = self.name.text()
        s_name = self.second_name.text()
        student = s_name+' '+name
        student = QtWidgets.QTableWidgetItem(student)
        group =  self.group.text()
        group = QtWidgets.QTableWidgetItem(group)

        # Add to table
        self.main_window.students_table.insertRow(self.main_window.students_table.rowCount())
        row_count=self.main_window.students_table.rowCount()
        self.main_window.students_table.setItem(row_count-1, 0,student)
        self.main_window.students_table.setItem(row_count-1, 1,group)

        #Clear Table
        self.name.clear()
        self.second_name.clear()
        self.group.clear()
        self.close()


class Redactor (QtWidgets.QMainWindow, redactor.Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__()
        self.setupUi(self)
        self.setWindowTitle('Redactor')
        self.selected_row=None;

        #connect with main window 
        self.main_window = Table()

        #button to save info 
        self.pushButton.clicked.connect(self.save)

        #button to remove student from table
        self.pushButton_2.clicked.connect(self.remove)


    def save(self):
        #Delete old info 
        self.main_window.students_table.takeItem(self.selected_row, 0)
        self.main_window.students_table.takeItem(self.selected_row, 1)

        # Get Text
        name = self.name.text()
        s_name = self.second_name.text()
        student = s_name+' '+name
        student = QtWidgets.QTableWidgetItem(student)
        group =  self.group.text()
        group = QtWidgets.QTableWidgetItem(group)

        # Add to table
        row_count=self.main_window.students_table.rowCount()
        self.main_window.students_table.setItem(self.selected_row, 0,student)
        self.main_window.students_table.setItem(self.selected_row, 1,group)

        #Clear Table
        self.name.clear()
        self.second_name.clear()
        self.group.clear()
        self.close()
    
    def remove(self):
        # Delete row
        self.main_window.students_table.removeRow(self.selected_row)

        #Clear Table
        self.name.clear()
        self.second_name.clear()
        self.group.clear()
        self.close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    demo = Table()
    demo.show()
    sys.exit(app.exec_())

