from app import *
from FramelessWindow import *


if __name__ == '__main__':
    import sys

    import cgitb
    sys.excepthook = cgitb.enable(1, None, 5, '')
    app = QApplication(sys.argv)
    app.setStyleSheet(Style)
    window = TestCTitleBarWidget()
    window.setWindowTitle('Практическкие работы')
    window.show()
    sys.exit(app.exec_())
